//
//  StpBlueRC.m
//  BlueRC
//
//  Created by Jason Peterson on 4/6/13.
//
//

#import "StpBlueRC.h"

#pragma mark - UUID to String

@interface CBUUID (StringExtraction)

- (NSString *)representativeString;

@end

@implementation CBUUID (StringExtraction)

- (NSString *)representativeString;
{
    NSData *data = [self data];
    
    NSUInteger bytesToConvert = [data length];
    const unsigned char *uuidBytes = [data bytes];
    NSMutableString *outputString = [NSMutableString stringWithCapacity:16];
    
    for (NSUInteger currentByteIndex = 0; currentByteIndex < bytesToConvert; currentByteIndex++)
    {
        switch (currentByteIndex)
        {
            case 3:
            case 5:
            case 7:
            case 9:[outputString appendFormat:@"%02x-", uuidBytes[currentByteIndex]]; break;
            default:[outputString appendFormat:@"%02x", uuidBytes[currentByteIndex]];
        }
        
    }
    
    return outputString;
}

@end

#pragma mark - NSData to Hex String

@interface NSData (NSData_Conversion)

- (NSString *)hexadecimalString;

@end

@implementation NSData (NSData_Conversion)

- (NSString *)hexadecimalString {
    /* Returns hexadecimal string of NSData. Empty string if data is empty.   */
    
    const unsigned char *dataBuffer = (const unsigned char *)[self bytes];
    
    if (!dataBuffer)
        return [NSString string];
    
    NSUInteger          dataLength  = [self length];
    NSMutableString     *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
    
    for (int i = 0; i < dataLength; ++i)
        [hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];
    
    return [NSString stringWithString:hexString];
}

@end

#pragma mark - Start StpBlueRC

@implementation StpBlueRC

@synthesize foundPeripherals;
@synthesize tempCharacteristic;
@synthesize characteristicChannelArray;
@synthesize connectingPeripheral;
@synthesize isConnecting;
@synthesize packetTimer;
@synthesize serverSocket;
@synthesize connectedSockets;
@synthesize blePackets;
@synthesize servoCache;
@synthesize lastPacketTime;

+ (id) sharedInstance
{
	static StpBlueRC	*this = nil;
    
	if (!this)
		this = [[StpBlueRC alloc] init];
    
	return this;
}

-(void) packetTimerFire:(NSTimer *)timer
{
    if([blePackets count] != 0)
    {
        //NSLog(@"packetTimerFire");
        [[NSNotificationCenter defaultCenter]  postNotificationName:@"SendData" object:nil userInfo:[blePackets objectAtIndex:0]];
        [blePackets removeObjectAtIndex:0];
    }
}

- (id) init
{
    self = [super init];
    if (self) {
        NSLog(@"StpBlueRC.init");
        pollCount = 0;
		pendingInit = YES;
		centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue()];
        foundPeripherals = [[NSMutableArray alloc] init];
        blePackets = [[NSMutableArray alloc] initWithCapacity:30];
        servoCache = [[NSMutableDictionary alloc] initWithCapacity:16];
        characteristicChannelArray = [[NSMutableDictionary alloc] init];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendData:) name:@"SendData" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(queueData:) name:@"QueueData" object:nil];
        connectedSockets = [[NSMutableArray alloc] initWithCapacity:5];
        serverSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
        NSError *error = nil;
        
        if(![serverSocket acceptOnPort:5555 error:&error]){
            NSLog(@"Error starting server (bind): %@", error);
        }
        
        packetTimer = [NSTimer scheduledTimerWithTimeInterval:0.020 target:self selector:@selector(packetTimerFire:) userInfo:nil repeats:YES];
        lastPacketTime = [NSDate date];
        
      	}
    return self;
}

- (void) dealloc
{
    // We are a singleton and as such, dealloc shouldn't be called.
    assert(NO);
}

#pragma mark - Send Data to Receiver
-(void)sendData:(NSNotification *) thisNotification{
    
    NSDictionary * kvp = [thisNotification userInfo];
    NSData *data = [kvp objectForKey:@"Data"];
    NSString* characteristic = [kvp objectForKey:@"Characteristic"];
    NSNumber* withResponse = [kvp objectForKey:@"WithResponse"];
    
    CBPeripheral *peripheral;
    for(peripheral in foundPeripherals)
    {
        if([[self characteristicChannelArray] objectForKey:characteristic] != nil)
        {
            CBCharacteristic *thisCharacteristic = [[self characteristicChannelArray] objectForKey:characteristic];
            int dataLength = [data length];
            if (dataLength > 20)
                dataLength = 20;
            
            //NSLog(@"%d", dataLength);
            
            NSMutableData *packet = [[NSMutableData alloc] initWithCapacity:dataLength];
            char buffer[dataLength];
            [data getBytes:buffer length:dataLength];
            [packet replaceBytesInRange:NSMakeRange(0, dataLength) withBytes:buffer];
            
            //NSLog(@"%@", [packet hexadecimalString]);
            
            if([withResponse boolValue])
                [peripheral writeValue:packet forCharacteristic:thisCharacteristic type:CBCharacteristicWriteWithResponse];
            else
                [peripheral writeValue:packet forCharacteristic:thisCharacteristic type:CBCharacteristicWriteWithoutResponse];
        }
        
    }
}

-(void)queueData:(NSNotification *) thisNotification{
    [blePackets addObject:[thisNotification userInfo]];
}

/*
-(void)sendData:(NSNotification *) thisNotification{
    
    NSDictionary * kvp = [thisNotification userInfo];
    NSData *data = [kvp objectForKey:@"Data"];
    NSString* dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSArray *channelData = [dataString componentsSeparatedByString:@":"];
    
    if ([channelData count] != 3)
        return;
    
    int channel = [[channelData objectAtIndex:0] intValue];
    int startValue = [[channelData objectAtIndex:1] intValue];
    int stopValue = [[channelData objectAtIndex:2] intValue];
    
    if(channel < 1 || startValue < 0 || startValue > 4095 || stopValue < 0 || stopValue > 4095 || stopValue <= startValue)
        return;
    
    channel = ((channel - 1) * 4) + 6;
    
    CBPeripheral *peripheral;
    for(peripheral in foundPeripherals)
    {
        if([[self characteristicChannelArray] objectForKey:@"Channel Control"] != nil)
        {
            NSMutableData *packet = [[NSMutableData alloc] initWithCapacity:5];
            char buffer[5];
            
            buffer[0] = (char) channel;
            buffer[1] = (char) startValue;
            buffer[2] = (char) (startValue>>8);
            buffer[3] = (char) stopValue;
            buffer[4] = (char) (stopValue>>8);
            
            [packet replaceBytesInRange:NSMakeRange(0, 5) withBytes:buffer];
            
            [peripheral writeValue:packet forCharacteristic:[[self characteristicChannelArray] objectForKey:@"Channel Control"] type:CBCharacteristicWriteWithoutResponse];
        }
        
    }
}
*/

#pragma mark - Restoring
/****************************************************************************/
/*								Settings									*/
/****************************************************************************/
/* Reload from file. */
- (void) loadSavedDevices
{
    NSLog(@"StpBlueRC.loadSavedDevices");
	NSArray	*storedDevices	= [[NSUserDefaults standardUserDefaults] arrayForKey:@"StoredDevices"];
    
	if (![storedDevices isKindOfClass:[NSArray class]]) {
        NSLog(@"No stored array to load");
        return;
    }
    
    for (id deviceUUIDString in storedDevices) {
        
        if (![deviceUUIDString isKindOfClass:[NSString class]])
            continue;
        
        CFUUIDRef uuid = CFUUIDCreateFromString(NULL, (CFStringRef)deviceUUIDString);
        if (!uuid)
            continue;
        
        [self retrievePeripherals:[NSArray arrayWithObject:(id)CFBridgingRelease(uuid)]];
        
        CFRelease(uuid);
    }
    
}


- (void) addSavedDevice:(CFUUIDRef) uuid
{
    NSLog(@"StpBlueRC.addSavedDevice");
	NSArray			*storedDevices	= [[NSUserDefaults standardUserDefaults] arrayForKey:@"StoredDevices"];
	NSMutableArray	*newDevices		= nil;
	CFStringRef		uuidString		= NULL;
    
	if (![storedDevices isKindOfClass:[NSArray class]]) {
        NSLog(@"Can't find/create an array to store the uuid");
        return;
    }
    
    newDevices = [NSMutableArray arrayWithArray:storedDevices];
    
    uuidString = CFUUIDCreateString(NULL, uuid);
    if (uuidString) {
        [newDevices addObject:(NSString*)CFBridgingRelease(uuidString)];
        CFRelease(uuidString);
    }
    /* Store */
    [[NSUserDefaults standardUserDefaults] setObject:newDevices forKey:@"StoredDevices"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (void) removeSavedDevice:(CFUUIDRef) uuid
{
    NSLog(@"StpBlueRC.removeSavedDevice");
	NSArray			*storedDevices	= [[NSUserDefaults standardUserDefaults] arrayForKey:@"StoredDevices"];
	NSMutableArray	*newDevices		= nil;
	CFStringRef		uuidString		= NULL;
    
	if ([storedDevices isKindOfClass:[NSArray class]]) {
		newDevices = [NSMutableArray arrayWithArray:storedDevices];
        
		uuidString = CFUUIDCreateString(NULL, uuid);
		if (uuidString) {
			[newDevices removeObject:(NSString*)CFBridgingRelease(uuidString)];
            CFRelease(uuidString);
        }
		/* Store */
		[[NSUserDefaults standardUserDefaults] setObject:newDevices forKey:@"StoredDevices"];
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
}


#pragma mark - Connection Helpers
- (void) clearDevices
{
    NSLog(@"StpBlueRC.clearDevices");
    [characteristicChannelArray removeAllObjects];
    [foundPeripherals removeAllObjects];
}

- (void) connectPeripheral:(CBPeripheral*)peripheral
{
    NSLog(@"StpBlueRC.connectPeripheral");
	if (![peripheral isConnected]) {
		[centralManager connectPeripheral:peripheral options:nil];
	}
}


- (void) disconnectPeripheral:(CBPeripheral*)peripheral
{
    NSLog(@"StpBlueRC.disconnectPeripheral");
	[centralManager cancelPeripheralConnection:peripheral];
}

- (void) retrievePeripherals:(NSArray *)peripheralUUIDs{
    
    /*
    if(!peripheralUUIDs)
        [centralManager retrieveConnectedPeripherals];
    else
        [centralManager retrievePeripherals:peripheralUUIDs];
     */
    
    [self startScanningForUUIDString:@"e4377d72-3993-43d7-a941-cd96530783a4"];
}

- (void) addPeripheral:(CBPeripheral *)peripheral{
    
    if (![foundPeripherals containsObject:peripheral]) {
        [self stopScanning];
        [self setIsConnecting:FALSE];
		[foundPeripherals addObject:peripheral];
	}
}


#pragma mark - Discovery
/****************************************************************************/
/*								Discovery                                   */
/****************************************************************************/
- (void) startScanningForUUIDString:(NSString *)uuidString
{
    NSLog(@"StpBlueRC.startScanningForUUIDString");
    
	NSArray *uuidArray	= nil;
    
    if(uuidString != nil)
        uuidArray = [NSArray arrayWithObjects:[CBUUID UUIDWithString:uuidString], nil];
    
	NSDictionary	*options	= [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:CBCentralManagerScanOptionAllowDuplicatesKey];
    
	[centralManager scanForPeripheralsWithServices:uuidArray options:options];
}

- (void) stopScanning
{
    NSLog(@"StpBlueRC.stopScanning");
	[centralManager stopScan];
}


#pragma mark - CentralManager Delegate
/****************************************************************************/
/*						Connection/Disconnection                            */
/****************************************************************************/

- (void) centralManager:(CBCentralManager *)central didRetrieveConnectedPeripherals:(NSArray *)peripherals
{
    NSLog(@"StpBlueRC.didRetrieveConnectedPeripherals %d", [peripherals count]);
	CBPeripheral	*peripheral;
	
	/* Add to list. */
	for (peripheral in peripherals)
    {
        CFStringRef		uuidString		= NULL;
        
        uuidString = CFUUIDCreateString(NULL, [peripheral UUID]);
        
        if (uuidString)
            NSLog(@"StpBlueRC.didRetrieveConnectedPeripherals discovering services device UUID = %@", (NSString*)CFBridgingRelease(uuidString));
        else
            NSLog(@"StpBlueRC.didRetrieveConnectedPeripherals discovering services");
        
        //[peripheral discoverServices:[NSArray arrayWithObjects:[CBUUID UUIDWithString:@"e4377d72-3993-43d7-a941-cd96530783a4"], nil]];
        
        [centralManager connectPeripheral:peripheral options:nil];
	}
}


- (void) centralManager:(CBCentralManager *)central didRetrievePeripheral:(CBPeripheral *)peripheral
{
    CFStringRef		uuidString		= NULL;
    
    uuidString = CFUUIDCreateString(NULL, [peripheral UUID]);
    
    if (uuidString)
        NSLog(@"StpBlueRC.didRetrievePeripheral discovering services UUID = %@", (NSString*)CFBridgingRelease(uuidString));
    else
        NSLog(@"StpBlueRC.didRetrievePeripheral discovering services device");
    
    //[peripheral discoverServices:[NSArray arrayWithObjects:[CBUUID UUIDWithString:@"e4377d72-3993-43d7-a941-cd96530783a4"], nil]];
    
    [centralManager connectPeripheral:peripheral options:nil];
}


- (void) centralManager:(CBCentralManager *)central didFailToRetrievePeripheralForUUID:(CFUUIDRef)UUID error:(NSError *)error
{
    NSLog(@"StpBlueRC.didFailToRetrievePeripheralForUUID");

}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    
    CFStringRef		uuidString		= NULL;
    
    //Hack for simulator...
    if(![peripheral UUID])
        return;
    
    if([self isConnecting])
        return;
    
    uuidString = CFUUIDCreateString(NULL, [peripheral UUID]);
    
    if (uuidString)
        NSLog(@"StpBlueRC.didDiscoverPeripheral for device UUID = %@", (NSString*)CFBridgingRelease(uuidString));
    else
        NSLog(@"StpBlueRC.didDiscoverPeripheral ");
    
    //[peripheral discoverServices:[NSArray arrayWithObjects:[CBUUID UUIDWithString:@"e4377d72-3993-43d7-a941-cd96530783a4"], nil]];
    
        
    NSArray * serviceUUIDs = [advertisementData objectForKey:CBAdvertisementDataServiceUUIDsKey];
    
    for (CBUUID *foundServiceUUID in serviceUUIDs)
    {
        
        if ([foundServiceUUID isEqual:[CBUUID UUIDWithString:@"e4377d72-3993-43d7-a941-cd96530783a4"]])
        {
            NSLog(@"StpBlueRC.didDiscoverPeripheral.foundValidPeripheral ");
            self.connectingPeripheral = peripheral;
            [self setIsConnecting:TRUE];
            [centralManager connectPeripheral:peripheral options:nil];
            break;
        }
    }
    
    
	
}


- (void) centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    CFStringRef		uuidString		= NULL;
    
    uuidString = CFUUIDCreateString(NULL, [peripheral UUID]);
    
    if (uuidString)
        NSLog(@"StpBlueRC.didConnectPeripheral for device UUID = %@", (NSString*)CFBridgingRelease(uuidString));
    else
        NSLog(@"StpBlueRC.didConnectPeripheral ");

    
    [peripheral setDelegate:self];
    [peripheral discoverServices:[NSArray arrayWithObjects:[CBUUID UUIDWithString:@"e4377d72-3993-43d7-a941-cd96530783a4"], nil]];
   
}

- (void) centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@"Attempted connection to peripheral %@ failed: %@", [peripheral name], [error localizedDescription]);
    [self setIsConnecting:FALSE];
}


- (void) centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    if(error)
        NSLog(@"StpBlueRC.didDisconnectPeripheral.ERROR: %@", error);
    
    CFStringRef		uuidString		= NULL;
    
    uuidString = CFUUIDCreateString(NULL, [peripheral UUID]);
    
    if (uuidString)
        NSLog(@"StpBlueRC.didDisconnectPeripheral for device UUID = %@", (NSString*)CFBridgingRelease(uuidString));
    else
        NSLog(@"StpBlueRC.didDisconnectPeripheral ");
    
    [centralManager cancelPeripheralConnection:peripheral];
    
    [self clearDevices];
    [self retrievePeripherals:nil];
}


- (void) centralManagerDidUpdateState:(CBCentralManager *)central
{
    NSLog(@"StpBlueRC.centralManagerDidUpdateState");
    
    static CBCentralManagerState previousState = -1;
    
	switch ([centralManager state]) {
		case CBCentralManagerStatePoweredOff:
		{
            [self clearDevices];
            
			/* Tell user to power ON BT for functionality, but not on first run - the Framework will alert in that instance. */
            if (previousState != -1) {
                
            }
			break;
		}
            
		case CBCentralManagerStateUnauthorized:
		{
			/* Tell user the app is not allowed. */
			break;
		}
            
		case CBCentralManagerStateUnknown:
		{
			/* Bad news, let's wait for another event. */
			break;
		}
            
		case CBCentralManagerStatePoweredOn:
		{
			pendingInit = NO;
			[self loadSavedDevices];
			[self retrievePeripherals:nil];
			break;
		}
            
		case CBCentralManagerStateResetting:
		{
			[self clearDevices];
			pendingInit = YES;
			break;
		}
            
        case CBCentralManagerStateUnsupported:
        {
            
            break;
        }
	}
    
    previousState = [centralManager state];
}

#pragma mark - Peripheral Delegate

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error{
    
    NSString *uuid = [[service UUID] representativeString];
    NSLog(@"StpBlueRC.peripheral.didDiscoverCharacteristicsForService for service UUID = 0x%@", uuid);
    
    if([uuid isEqualToString:@"e4377d72-3993-43d7-a941-cd96530783a4"])
    {
        CBCharacteristic * characteristic;
        for(characteristic in [service characteristics])
        {
            NSString *cUuid = [[characteristic UUID] representativeString];
            
            NSLog(@"StpBlueRC.peripheral.didDiscoverCharacteristicsForService for service %@ UUID = 0x%@", uuid, cUuid);
            
            [peripheral discoverDescriptorsForCharacteristic:characteristic];
            
            /*
            if([cUuid isEqualToString:@"8936c87a-9d67-43f3-9fe8-b292743f47ce"])
            {
                NSLog(@"StpBlueRC.peripheral.didDiscoverCharacteristicsForService saving Channel Control Verified characteristic...");
                [[self characteristicChannelArray] setObject:characteristic forKey:@"Channel Control Verified"];
                //[peripheral setNotifyValue:YES forCharacteristic:characteristic];
            }
            
            if([cUuid isEqualToString:@"ec123c55-031a-418a-9997-26764dd22c75"])
            {
                NSLog(@"StpBlueRC.peripheral.didDiscoverCharacteristicsForService saving Frequency Prescaler characteristic...");
                [[self characteristicChannelArray] setObject:characteristic forKey:@"Frequency Prescaler"];
                //[peripheral setNotifyValue:YES forCharacteristic:characteristic];
            }
            
            if([cUuid isEqualToString:@"882b4cf2-80ec-46d7-bf99-301ade180c91"])
            {
                NSLog(@"StpBlueRC.peripheral.didDiscoverCharacteristicsForService saving Channel Control characteristic...");
                [[self characteristicChannelArray] setObject:characteristic forKey:@"Channel Control"];
                //[peripheral setNotifyValue:YES forCharacteristic:characteristic];
            }
            
            if([cUuid isEqualToString:@"c6e55d12-804a-4c88-bf0a-27c58fa37638"])
            {
                NSLog(@"StpBlueRC.peripheral.didDiscoverCharacteristicsForService saving Channel Default Value characteristic...");
                [[self characteristicChannelArray] setObject:characteristic forKey:@"Channel Default Value"];
                //[peripheral setNotifyValue:YES forCharacteristic:characteristic];
            }
             */
                
            
        }
        
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    if(error)
        NSLog(@"StpBlueRC.peripheral.didDiscoverDescriptorsForCharacteristic.ERROR: %@", error);
    else
    {
        NSLog(@"StpBlueRC.peripheral.didDiscoverDescriptorsForCharacteristic");
        CBDescriptor * descriptor;
        for(descriptor in [characteristic descriptors])
        {
               if([[[descriptor UUID] description] isEqualToString:@"Characteristic User Description"])
               {
                   [peripheral readValueForDescriptor:descriptor];
               
               }
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverIncludedServicesForService:(CBService *)service error:(NSError *)error{
    if(error)
        NSLog(@"StpBlueRC.peripheral.didDiscoverIncludedServicesForService.ERROR: %@", error);
    else
        NSLog(@"StpBlueRC.peripheral.didDiscoverIncludedServicesForService");
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error{
    
    if(error)
    {
        NSLog(@"StpBlueRC.didDiscoverServices.ERROR: %@", error);
        return;
    }
    
    [self addPeripheral:peripheral];
    
    CFStringRef		uuidString		= NULL;
    
    uuidString = CFUUIDCreateString(NULL, [peripheral UUID]);
    
    if (uuidString)
        NSLog(@"StpBlueRC.peripheral.didDiscoverServices for device UUID = %@", (NSString*)CFBridgingRelease(uuidString));
    else
        NSLog(@"StpBlueRC.peripheral.didDiscoverServices ");
    
    NSArray *currentServices = [[NSArray alloc] initWithArray:[peripheral services]];
    
    for (int i = 0; i < [currentServices count]; i++) {
        CBService *service = [currentServices objectAtIndex:i];
        [peripheral discoverCharacteristics:nil forService:service];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    if(error)
        NSLog(@"StpBlueRC.peripheral.didUpdateNotificationStateForCharacteristic.ERROR: %@", error);
    else
        NSLog(@"StpBlueRC.peripheral.didUpdateNotificationStateForCharacteristic");
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    
    if(error)
        NSLog(@"StpBlueRC.peripheral.didUpdateValueForCharacteristic.ERROR: %@", error);
    else
        NSLog(@"StpBlueRC.peripheral.didUpdateValueForCharacteristic");
    
    NSString *cUuid = [[characteristic UUID] representativeString];
    //NSLog(@"StpBlueRC.peripheral.didUpdateValueForCharacteristic for characteristic UUID = 0x%@", cUuid);
    
    
    if([cUuid isEqualToString:@"8936c87a-9d67-43f3-9fe8-b292743f47ce"])
    {
        NSData* temp = [characteristic value];
        
        if ([temp length])
        {
            //NSLog(@"SBS - ReceivedData");
            //NSString *someString = [[NSString alloc] initWithData:temp encoding:NSASCIIStringEncoding];
            //NSLog(@"%@", someString);
            //[[NSNotificationCenter defaultCenter] postNotificationName:@"ReceivedData" object:self userInfo:[NSDictionary dictionaryWithObject:temp forKey:@"Data"]];
        }
    }
    
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForDescriptor:(CBDescriptor *)desciptor error:(NSError *)error{
    
    if(error)
    {
        NSLog(@"StpBlueRC.peripheral.didUpdateValueForDescriptor.ERROR: %@", error);
    }
    else
    {
        
        if([[[desciptor UUID] description] isEqualToString:@"Characteristic User Description"])
        {
            NSLog(@"StpBlueRC.peripheral.didUpdateValueForDescriptor %@, %@", [desciptor UUID], [desciptor value] );
            [[self characteristicChannelArray] setObject:[desciptor characteristic] forKey:[desciptor value]];
            
        }
        
    }
    

}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    if(error)
        NSLog(@"StpBlueRC.peripheral.didWriteValueForCharacteristic.ERROR: %@", error);
    
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error{
    if(error)
        NSLog(@"StpBlueRC.peripheral.didWriteValueForDescriptor.ERROR: %@", error);
    else
        NSLog(@"StpBlueRC.peripheral.didWriteValueForDescriptor");
}

- (void)peripheralDidUpdateRSSI:(CBPeripheral *)peripheral error:(NSError *)error{
    if(error)
    {
        //NSLog(@"StpBlueRC.peripheralDidUpdateRSSI.ERROR: %@", error);
    }
    else
    {
        //NSLog(@"StpBlueRC.peripheralDidUpdateRSSI = %d dB", [[peripheral RSSI] intValue]);
    }
}

#pragma mark - Socket Server Delegate

- (void)socket:(GCDAsyncSocket *)sock didAcceptNewSocket:(GCDAsyncSocket *)newSocket
{
    NSLog(@"Accepted new socket from %@:%hu", [newSocket connectedHost], [newSocket connectedPort]);

    [connectedSockets addObject:newSocket];
    //NSString *welcomMessage = @"Hello from the server\r\n";
    //[newSocket writeData:[welcomMessage dataUsingEncoding:NSUTF8StringEncoding] withTimeout:-1 tag:1];
    
    [newSocket performBlock:^{
        [newSocket enableBackgroundingOnSocket];
    }];
    [newSocket readDataWithTimeout:-1 tag:0];
    
}

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{
    
    [sock readDataWithTimeout:-1 tag:0];
    
    NSString* characteristic = @"Fast Servo Control";
    
    char dataBuffer[2];
    [data getBytes:dataBuffer length:2];
    
    
    NSNumber * channel = [NSNumber numberWithChar:dataBuffer[0] + 1];
    
    [servoCache setObject:data forKey:channel];
    
    char packetBuffer[9];
    char offset = 0;
    
    if ([channel charValue] <= 8)
    {
        packetBuffer[0] = 6;
    }
    else
    {
        offset = 8;
        packetBuffer[0] = 38;
    }
    
    
    for(char i = (1 + offset); i <= (8 + offset); i++)
    {
        channel = [NSNumber numberWithChar:i];
        data = [servoCache objectForKey:channel];
        if(data != nil)
        {
            [data getBytes:dataBuffer length:2];
            packetBuffer[i - offset] = dataBuffer[1];
        }
        else
        {
            packetBuffer[i - offset] = 128;
        }
        
    }
    
    NSData *packet = [NSMutableData dataWithBytes:packetBuffer length:9];
    
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:characteristic, packet, [NSNumber numberWithBool:false], nil] forKeys:[NSArray arrayWithObjects:@"Characteristic", @"Data", @"WithResponse", nil]];
    
    double timePassed_ms = [lastPacketTime timeIntervalSinceNow] * -1000.0;
    
    //if(timePassed_ms >= 20)
        [blePackets addObject:dict];
    
    lastPacketTime = [NSDate date];
    
}

@end

