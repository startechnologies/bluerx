Ext.define('BlueRC.view.Joystick', {
    extend:'Ext.Container',
    xtype:'joystick',

    config:{
        margin:10,
        layout:{
            type:'vbox',
            pack:'center',
            align:'stretch'
        },

        items:[
            {
                style:{
                    textAlign: 'center',
                    background: 'url(../www/img/steeringWheel.png) no-repeat center',
                    backgroundSize: 'contain',
                    width:'100%',
                    height:'100%'
                }
            }
        ]
    }
});