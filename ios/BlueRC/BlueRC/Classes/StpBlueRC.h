//
//  StpBlueRC.h
//  BlueRC
//
//  Created by Jason Peterson on 4/6/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "GCDAsyncSocket.h"
#import <ifaddrs.h>
#import <arpa/inet.h>

@interface StpBlueRC : NSObject <CBCentralManagerDelegate, CBPeripheralDelegate> {
    CBCentralManager *centralManager;
    BOOL pendingInit;
    BOOL isUploading;
    int lastUploadIndex;
    int pollCount;
    NSMutableDictionary *characteristicChannelArray;
}

@property (nonatomic, strong) NSMutableArray * foundPeripherals;
@property (nonatomic, strong) CBCharacteristic *tempCharacteristic;
@property (nonatomic, strong) NSMutableDictionary *characteristicChannelArray;
@property (strong) CBPeripheral *connectingPeripheral;
@property (readwrite) BOOL isConnecting;
@property (strong) NSTimer *packetTimer;
@property (nonatomic, strong) GCDAsyncSocket* serverSocket;
@property (nonatomic, strong) NSMutableArray* connectedSockets;
@property (nonatomic, strong) NSMutableArray* blePackets;
@property (nonatomic, strong) NSMutableDictionary *servoCache;
@property (nonatomic, strong) NSDate* lastPacketTime;

+ (id) sharedInstance;
- (id) init;
- (void) startScanningForUUIDString:(NSString *)uuidString;

@end
